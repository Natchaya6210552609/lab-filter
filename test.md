#### [Main Page](../README.md)

## Lowpass Filter :

Definition of Lowpass filter

A low-pass filter is a filter that passes signals with a frequency lower than a selected cutoff frequency and attenuates signals with frequencies higher than the cutoff frequency.


In this project, we design the Butterworth LC Low pass filter with cut-off frequency at 5.0 GHz and attenuation equal to 30 dB at 10 GHz. 

## Parameter list
   - cutoff frequency $`(f_c) = 5`$ GHz.  

   - cutoff omega $`(ω_c) = 2\pi f_c = 3.14E10`$ rad/sec  

   - Frequency with attenuation equal to $`30`$ at $`10`$ GHz


## Find order of LC filter

From figure 1, this shows a relationship between Attenuation versus normalized frequency for flat filter prototypes. We could see that, if we know the value of

```math
\mathbf |\frac{ω}{ω_c}| -1
 ```

then we can know which one should be the order of our filter.

```math
\mathbf |\frac{ω}{ω_c}| -1   =  |\frac{10}{5}| -1   =   1
```
Take a look at the graph where  $`|\frac{ω}{ω_c}| -1 = 1`$ and Attenuation = $`30`$, we got $`n = 5`$ which means our filter has $`5`$ order.

<img src="pic/111.jpg" width="50%" height="50%"> 

   figure 1  

## Find value of each inductor and capacitor in filter
From table 8.3, this shows an Element values for Maximally Flat Low-Pass Filter Prototypes when cutoff omega $`(ω_c) = 1`$ rad/sec and load impedance $` = 1 \Omega `$.  

<img src="pic/222.jpg" width="50%" height="50%">  

The last parameter in each order is a load impedance and the rest is the alternating value of inductor and capacitor. In this project, we should Inductor as our first element and again, our filter is $`5`$ so we got the parameter like this  

<img src="pic/T1.jpg" width="50%" height="50%">   

Table 1  

Those are the values when load impedance $` = 1 \Omega `$ but in this project, we use load impedance $` = 50 \Omega `$ so the inductor needs to be multiplied by $`50`$ but for the capacitor it needs to be divided by  $`50`$.  

<img src="pic/T2.jpg" width="50%" height="50%">  

Table 2  

Also, we need to change the cutoff omega to $`3.14E10`$ rad/sec so all elements need to be divided by $`3.14E10`$.  

<img src="pic/T3.jpg" width="50%" height="50%">  

Table 3  

## Design LC low pass in Sonnet Software.  
Our LC low pass filter will look like figure 2 with a 5 order filter and inductor as the first element.

<img src="pic/333.jpg" width="50%" height="50%">  

figure 2  

Then we set all elements with the value in table 3 and also set the node.  

<img src="pic/555.jpeg" width="50%" height="50%">  

After that we set the range of frequency that we want to simulate.  

<img src="pic/444.jpg" width="50%" height="50%">  

And this is our result graph. 

<img src="pic/666.jpg" width="50%" height="50%">  

When the red line is power from port 1 to port 1 and the blue line is power from port 1 to port 2.  

If we zoom in to the beginning of the red line, we could see that there is the reflected power about -75 dB. This amount is very small so this won’t cause any trouble. 

<img src="pic/E1.jpg" width="50%" height="50%">  

At 5 GHz, we almost got attenuation equal to 3.  

<img src="pic/E2.jpg" width="50%" height="50%"> 

At 10 GHz, we also almost got attenuation equal to 30.  

<img src="pic/E3.jpg" width="50%" height="50%"> 

## Conclusion
	
We didn’t get the wanted attenuation but its error is not that much. If we convert back to the Linear Magnitude scale, its value is just slightly different from what we wanted. Maybe our element values are not fine enough, that's why there is some error.






















